package com.cargohop.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.cargohop.bookingFlow.CancelBookingActivity;
import com.cargohop.utility.SessionManager;
import com.cargohop.interfaceMgr.CancelReasonInterface;
import com.cargohop.model.CancelBookingModel;
import com.cargohop.customer.R;

/**
 * <h1>CancelBookingController</h1>
 * <h4>This is a controller class for CancelBookingActivity Activity</h4>
 * This class is used for performing the task related to business logic and give a call to its model class.
 * @version 1.0
 * @author Shubham
 * @since 29/08/17
 * @see CancelBookingActivity
 */
public class CancelBookingController {

    private Activity context;
    private SessionManager sessionManager;
    private CancelBookingModel model;

    public CancelBookingController(Activity context, SessionManager sessionManager)
    {
        this.context = context;
        this.sessionManager = sessionManager;
        model = new CancelBookingModel(context, sessionManager);
    }

    /**
     * This method will get a call of cancel reason method of model class.
     * @param bid
     * @param cancelReasonInterface
     */
    public void cancelReason(String bid, CancelReasonInterface cancelReasonInterface)
    {
        model.cancelReasons(bid, cancelReasonInterface);
    }

    /**
     * This method will show the alert and if user chooses the Yes then It will call a method Cancel booking method of model class.
     * @param bid
     * @param reason
     * @param status
     * @param amount
     */
    public void showAlert(final String bid, final String reason, final int status, String amount)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_ok_cancel);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
        TextView tv_yes = dialog.findViewById(R.id.tv_yes);
        TextView tv_no = dialog.findViewById(R.id.tv_no);
        TextView tv_text = dialog.findViewById(R.id.tv_text);
        if (!amount.equals("0"))
            tv_text.setText(context.getString(R.string.charge)+" "+sessionManager.getCurrencySymbol()+amount+" "+context.getString(R.string.cancellation));
        else
            tv_text.setText(context.getString(R.string.chargeZero));
        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                model.cancelBooking(bid, reason, status);
            }
        });
        tv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
