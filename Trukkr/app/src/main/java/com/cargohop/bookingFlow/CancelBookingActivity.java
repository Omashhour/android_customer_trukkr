package com.cargohop.bookingFlow;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cargohop.adapter.CancelBookingAdapter;
import com.cargohop.controllers.CancelBookingController;
import com.cargohop.customer.R;
import com.cargohop.interfaceMgr.CancelReasonInterface;
import com.cargohop.pojos.CancelReasonDataPojo;
import com.cargohop.utility.SessionManager;
import com.cargohop.utility.Utility;

/**
 * <h1>CancelBookingActivity Activity</h1>
 * This class is used to Cancel the job.
 * @author 3embed
 * @since 3 Jan 2017.
 */
public class CancelBookingActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "CancelBookingActivity";
    RelativeLayout back_Layout;
    Button btn_cancel_booking;
    String cancel_reason = "";
    private String  bid;
    int status = 3;
    private CancelBookingController controller;
    private RecyclerView rv_cancel_booking_reasons;
    private String amount = "";

    /**
     * This is the onCreateHomeFrag method that is called firstly, when user came to login screen.
     * @param savedInstanceState contains an instance of Bundle.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_cancel);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);

        SessionManager sessionManager = new SessionManager(CancelBookingActivity.this);
        controller = new CancelBookingController(this, sessionManager);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            bid = bundle.getString("ent_bid");
        }

        //This is used for getting the Cancellation reason.
        controller.cancelReason(bid, new CancelReasonInterface() {
            @Override
            public void doProcess(CancelReasonDataPojo pojo) {
                CancelBookingAdapter cancelBookingAdapter = new CancelBookingAdapter(CancelBookingActivity.this, pojo);
                rv_cancel_booking_reasons.setAdapter(cancelBookingAdapter);
            }
        });

        initView();
    }
    /**
     * <h2>initView</h2>
     * <p>Initializing view elements</p>
     */
    private void initView()
    {
        back_Layout = findViewById(R.id.rlBackArrow);
        rv_cancel_booking_reasons = findViewById(R.id.rv_cancel_booking_reasons);
        btn_cancel_booking = findViewById(R.id.btn_cancel_booking);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false);
        rv_cancel_booking_reasons.setLayoutManager(linearLayoutManager);

        TextView signup_title = findViewById(R.id.tvToolBarTitle);
        signup_title.setText(getString(R.string.cancel_reason));

        ProgressDialog progressDialog = new ProgressDialog(CancelBookingActivity.this);
        progressDialog.setMessage(getString(R.string.canceling));
    }

    /**
     * This method is keep on calling each time
     */
    @Override
    protected void onResume() {
        super.onResume();
        back_Layout.setOnClickListener(this);
        btn_cancel_booking.setOnClickListener(this);
    }

    /**
     * This method is providing the onClick listener.
     * @param view contains the actual view.
     */
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.rlBackArrow:
                onBackPressed();
                break;
            case R.id.btn_cancel_booking:
                Utility.printLog(TAG+" cancel_reason "+cancel_reason+" amount "+amount);
                if (!cancel_reason.equals("") && !amount.equals(""))
                    controller.showAlert(bid, cancel_reason, status, amount);
                break;
        }
    }

    /**
     * This method will get the reason and amount from its adapter class.
     * @param reason cancel reason.
     * @param amount amount to be charged
     */
    public void cancelReason(String reason, String amount)
    {
        cancel_reason = reason;
        this.amount = amount;
    }

    /**onBack pressed close the activity.*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay_still, R.anim.slide_down_acvtivity);
    }
}
