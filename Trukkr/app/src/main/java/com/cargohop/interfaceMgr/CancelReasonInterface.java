package com.cargohop.interfaceMgr;

import com.cargohop.pojos.CancelReasonDataPojo;

/**
 * Created by embed on 30/8/17.
 */

public interface CancelReasonInterface {
    public void doProcess(CancelReasonDataPojo pojo);
}
