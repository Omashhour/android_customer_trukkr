package com.cargohop.interfaceMgr;

/**
 * Created by embed on 17/8/17.
 */

public interface ImageOperationInterface {
    public void onSuccess(String fileName);
    public void onFailure();
}
