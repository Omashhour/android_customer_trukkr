package com.cargohop.customer;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cargohop.utility.AppPermissionsRunTime;
import com.cargohop.utility.AppTypeface;
import com.cargohop.utility.Constants;
import com.cargohop.interfaceMgr.ResultInterface;
import com.cargohop.utility.SessionManager;
import com.cargohop.utility.Utility;
import com.cargohop.controllers.LoginController;
import com.cargohop.interfaceMgr.LoginInterface;
import com.cargohop.interfaceMgr.SpannableInterface;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
/**
 * <h1>Login Activity</h1>
 * This class is used to provide the Login screen, where we can do our login and if we forget our etNewPassword then here we also can make a request to forgot etNewPassword
 * and if login successful, then it directly opens Main Activity.
 * @author 3embed
 * @since 3 Jan 2017.*/
public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnFocusChangeListener,TextWatcher, OnTouchListener, GoogleApiClient.OnConnectionFailedListener
{
    private Resources resources;
    private TextView tvSignUp_login;
    private EditText etEmail_login, etPassword_login;
    private Button btnSignIn_login;
    private SessionManager sessionManager;
    private TextInputLayout tilEmail_login,password_Text_Rl;
    private CallbackManager callbackManager;
    int login_type = 1;
    private AppPermissionsRunTime permissionsRunTime;
    private ArrayList<AppPermissionsRunTime.Permission> permissionArrayList;
    private boolean login_flag = true;
    private LoginController loginController;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(LoginActivity.this);

        callbackManager = CallbackManager.Factory.create();
        loginController = new LoginController(this, sessionManager, callbackManager);
        setContentView(R.layout.activity_login);

        initialization();
        loginFirstTime();
    }

    /**
     * <h2>loginFirstTime</h2>
     * <p>
     * This method will check that either my app is already login before or not,
     * if it is login first time then keep both username and etNewPassword
     * field empty or else fill it with last time login details.
     * </p>
     */
    public void loginFirstTime()
    {
        loginController.firstTimeLogin(login_type, new ResultInterface() {
            @Override
            public void errorMandatoryNotifier()
            {
                etEmail_login.setText(sessionManager.getUserId());
                etPassword_login.setText(sessionManager.getPassword());
            }

            @Override
            public void errorInvalidNotifier() {
                Utility.printLog("Login is either first time or by using FB/Google.");
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(Utility.isNetworkAvailable(this))
        {
            Utility.printLog("network is connected.");
        }
        else
        {
            Utility.printLog("network is not connected.");
        }

        loginController.checkMail(etEmail_login.getText().toString());
        loginController.checkPassword(etPassword_login.getText().toString());
        isLoginBtnEnabled();
    }

    /**
     * <h2>onRequestPermissionsResult</h2>
     * <p>
     * This method got called, once we give any permission to our required permission.
     * </p>
     * @param requestCode  contains request code.
     * @param permissions   contains Permission list.
     * @param grantResults  contains the grant permission result.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.REQUEST_CODE)
        {
            boolean isAllGranted = true;
            for (String permissionTag : permissions)
            {
                if (permissionTag.equals(PackageManager.PERMISSION_GRANTED))
                {
                    isAllGranted = false;
                }
            }
            if (!isAllGranted)
            {
                permissionsRunTime.getPermission(permissionArrayList, this, true);
            }
            else
            {
                if (login_flag)
                    loginController.fbLogin();
                else
                    loginController.googleLogin();
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * <h2>initialization</h2>
     * <p>
     * This method is used to initializing all views of our layout.
     * </p>
     */
    private void initialization()
    {
        AppTypeface appTypeface = AppTypeface.getInstance(this);
        resources = getResources();

        TextView tvAppLogo =  findViewById(R.id.tvAppLogo);
        tvAppLogo.setTypeface(appTypeface.getPro_narMedium());

        TextView tvAppTagLine_login =  findViewById(R.id.tvAppTagLine_login);
        tvAppTagLine_login.setTypeface(appTypeface.getPro_News());

        tilEmail_login =  findViewById(R.id.tilEmail_login);
        tilEmail_login.setHint(resources.getString(R.string.login_email_phone_no));
        etEmail_login =  findViewById(R.id.etEmail_signUp);
        etEmail_login.setTypeface(appTypeface.getPro_News());
        etEmail_login.setOnFocusChangeListener(this);
        etEmail_login.addTextChangedListener(this);

        password_Text_Rl =  findViewById(R.id.tilPassword_login);
        password_Text_Rl.setHint(resources.getString(R.string.password));
        etPassword_login =  findViewById(R.id.etPassword_signUp);
        etPassword_login.setTypeface(appTypeface.getPro_News());
        etPassword_login.setOnFocusChangeListener(this);
        etPassword_login.addTextChangedListener(this);

        btnSignIn_login =  findViewById(R.id.btnSignIn_login);
        btnSignIn_login.setTypeface(appTypeface.getPro_News());
        btnSignIn_login.setEnabled(false);

        TextView tvForgotPassword =  findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setTypeface(appTypeface.getPro_News());

        tvSignUp_login = findViewById(R.id.tvSignUp_login);

        permissionsRunTime = AppPermissionsRunTime.getInstance();
        permissionArrayList = new ArrayList<AppPermissionsRunTime.Permission>();
        permissionArrayList.add(AppPermissionsRunTime.Permission.READ_EXTERNAL_STORAGE);

        createSpannable();
    }

    /**
     * <h2>createSpannable</h2>
     * <p>
     * This method is used to change color of "Sign Up" part in DON"T HAVE ACCOUNT?
     * SIGN UP and make it clickable.
     * </p>
     */
    public void createSpannable()
    {
        loginController.doSpannableOperation(new SpannableInterface() {
            @Override
            public void doProcess(SpannableString spannableString) {
                tvSignUp_login.setMovementMethod(LinkMovementMethod.getInstance());
                tvSignUp_login.setText(spannableString);
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Utility.hideSoftKeyBoard(v);
        return true;
    }

    /**
     * This is the method, where all onclick events, we can get and differentiate it based on their views.
     * @param v views
     */
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnSignIn_login:
                login_type = 1;
                loginController.doLogin(etEmail_login.getText().toString(), etPassword_login.getText().toString());
                break;

            case R.id.ivFbLoginBtn_login:
                login_flag = true;
                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (permissionsRunTime.getPermission(permissionArrayList, this, true))
                    {
                        loginController.fbLogin();
                    }
                }
                else
                {
                    loginController.fbLogin();
                }
                break;


            case R.id.ivGoogleLogin_login:
                login_flag = false;

                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (permissionsRunTime.getPermission(permissionArrayList, this, true))
                    {
                        loginController.googleLogin();
                    }
                }
                else
                {
                    loginController.googleLogin();
                }
                break;

            case R.id.tvForgotPassword:
                Intent intent=new Intent(this,ForgotPasswordActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
        {
            login_type = 2;
            etEmail_login.setText("");
            etPassword_login.setText("");
        }

        if (requestCode == Constants.RC_SIGN_IN)
        {
            login_type = 3;
            etEmail_login.setText("");
            etPassword_login.setText("");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            loginController.handleResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Utility.printLog("onConnectionFailed:" + connectionResult);
    }

    /**
     * <p>
     * This method got called, when focus got changed.
     * </p>
     * @param v contains the actual view.
     * @param hasFocus , focus is contained or not.
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {
        switch (v.getId())
        {
        case R.id.etEmail_signUp:
            if (!hasFocus) {
                // user is done editing
                loginController.validate_phone_email(etEmail_login.getText().toString(), new LoginInterface() {
                    @Override
                    public void onMail() {
                        tilEmail_login.setError(resources.getString(R.string.email_invalid));
                    }

                    @Override
                    public void onPhone() {
                        tilEmail_login.setError(resources.getString(R.string.phone_invalid));
                    }
                });
            }
        break;

            case R.id.etPassword_signUp:
            if (!hasFocus) {
                // user is done editing
                loginController.checkPasswordEmpty(etPassword_login.getText().toString(), new ResultInterface() {
                    @Override
                    public void errorMandatoryNotifier() {
                        password_Text_Rl.setError(resources.getString(R.string.password_mandatory));
                    }

                    @Override
                    public void errorInvalidNotifier() {
                        password_Text_Rl.setErrorEnabled(false);
                    }
                });
            }

            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    /**
     * <p>
     * This method got called, whenever user changes any text on EditText field.
     * </p>
     * @param editable Editable instance.
     */
    @Override
    public void afterTextChanged(Editable editable)
    {
        if (editable == etEmail_login.getEditableText())
        {
            tilEmail_login.setErrorEnabled(false);
            loginController.checkMail(etEmail_login.getText().toString());
            isLoginBtnEnabled();
        }
        if (editable == etPassword_login.getEditableText()) {
            password_Text_Rl.setErrorEnabled(false);

            loginController.checkPassword(etPassword_login.getText().toString());
            isLoginBtnEnabled();
        }
    }

    /**
     * <h2>isLoginBtnEnabled</h2>
     * <p>
     * This method is only used for enable/ disable the Login button and change their look.
     * </p>
     */
    private void isLoginBtnEnabled()
    {
        loginController.checkSignInEnabled(new ResultInterface() {
            @Override
            public void errorMandatoryNotifier() {
                btnSignIn_login.setEnabled(true);
                btnSignIn_login.setBackgroundResource(R.drawable.signin_login_selector);
            }

            @Override
            public void errorInvalidNotifier() {
                btnSignIn_login.setEnabled(false);
                btnSignIn_login.setBackgroundResource(R.drawable.grey_login_selector);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        finish();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
    }
}
