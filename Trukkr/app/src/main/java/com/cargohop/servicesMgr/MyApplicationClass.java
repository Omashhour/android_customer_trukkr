package com.cargohop.servicesMgr;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.cargohop.bookingHistory.ReceiptActivity;
import com.cargohop.pojos.StartReceiptActPojo;
import com.cargohop.utility.IsForeground;
import com.cargohop.utility.SessionManager;
import com.cargohop.utility.Utility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * <h>MyApplicationClass</h>
 * <P>
 *     Class to get the application class to
 *     handle application level apis and
 * </P>
 * @since 12/8/16.
 */
public class MyApplicationClass extends Application {
    private static MyApplicationClass applicationClass = null;
    SessionManager sessionManager;

    public static MyApplicationClass getInstance()
    {
        if (applicationClass == null)
        {
            applicationClass = new MyApplicationClass();
        }
        return applicationClass;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        sessionManager = new SessionManager(getApplicationContext());
        EventBus.getDefault().register(this);

        IsForeground.init(this);
        IsForeground.get(this).addListener(new IsForeground.Listener() {
            @Override
            public void onBecameForeground()
            {
                if(sessionManager.isLogin())
                    Utility.callConfig(getApplicationContext(), true);
            };

            @Override
            public void onBecameBackground()
            {
                Log.d("App_is_in_fb","B");
                PubNubMgr.getInstance().unSubscribePubNub();
            }
        });
    }


    @Override
    public void onTerminate()
    {
        EventBus.getDefault().unregister(this);
        super.onTerminate();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StartReceiptActPojo startReceiptActPojo)
    {
        /*if(!sessionManager.getHasReceiptActShown())
        {*/
        //sessionManager.setHasReceiptActShown(true);
        Intent receiptIntent = new Intent(getApplicationContext(), ReceiptActivity.class);
        Log.d("MyApplication", "bid:" +startReceiptActPojo.getBookingId());
        receiptIntent.putExtra("bid", startReceiptActPojo.getBookingId());
        //receiptIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        receiptIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(receiptIntent);
        //}
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
