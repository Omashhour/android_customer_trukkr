package com.cargohop.servicesMgr;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.cargohop.model.DataBaseHelper;
import com.cargohop.eventsHolder.WalletDataChangedEvent;
import com.cargohop.eventsHolder.WalletStatusChangedEvent;
import com.cargohop.customer.NotificationHandler;
import com.cargohop.customer.R;
import com.cargohop.pojos.DataBaseGetItemDetailPojo;
import com.cargohop.utility.Constants;
import com.cargohop.utility.Utility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

/**
 * <h1>FireBaseMessageReceiver</h1>
 * This class is for handling the messages those were came from FCM server.
 * @author 3embed
 * @since 6 Apr 2017.
 */
public class FireBaseMessageReceiver extends FirebaseMessagingService{
    PendingIntent intent = null;
    NotificationManager notificationManager;
    private static final String TAG = "FireBase_Message";
    private Bundle mbundle;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Utility.printLog(TAG+"onMessageReceived "+remoteMessage.getData());
        Utility.printLog(TAG+"onMessageReceived notification "+remoteMessage.getNotification());
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
        {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            try {
                //JSONObject jsonObject = new JSONObject();
                String str = new Gson().toJson(remoteMessage.getNotification());
                /*jsonObject.put("msg", remoteMessage.getNotification().getBody());
                jsonObject.put("st", remoteMessage.getNotification().get;
                jsonObject.put("msg", remoteMessage.getNotification().getBody()));
                jsonObject.put("msg", remoteMessage.getNotification().getBody()));
                jsonObject.put("msg", remoteMessage.getNotification().getBody()));*/

                Log.d(TAG, "Notification jsonObject: " + new Gson().toJson(str));
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
            sendNotifications();
            // app is in foreground, broadcast the push message
            /*Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", remoteMessage.getNotification().getBody());
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);*/
        }
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
            Log.e(TAG, "Notification Data: " + remoteMessage.getData().toString());
            handleMessage(remoteMessage.getData());
        }

        //Log.d(TAG, "From:" +remoteMessage.getFrom());
        //Log.d(TAG, "data: "+remoteMessage.getData());
        //JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        //check if message contains a data payload.

    }

    private void sendNotifications() {

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(getResources()
                        .getString(R.string.app_name))
                .setContentText("dtata")
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setStyle(new NotificationCompat.InboxStyle())
                .setContentIntent(intent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }

    /**
     * <h2>handleMessage</h2>
     * <p>
     *     method to parse and handle the data received from fcm
     * </p>
     * @param response: json object of the received object
     */
    private void handleMessage(Map response)
    {
        String status;
        String bid;
        String subid=null;
        String pay_load;
        String body,title;
        pay_load = String.valueOf(response.get("msg"));
        status = (String) response.get("st");
        bid = (String) response.get("bid");
        body= (String) response.get("body");
        title= (String) response.get("title");

        Utility.printLog("FCM isApplicationSentToBackground " + getBaseContext().getPackageName());
        boolean isbkrnd = isApplicationSentToBackground(getBaseContext());
        Utility.printLog("FCM isApplicationSentToBackground " + isbkrnd);

        Log.d("asdf", "notification: inside came: " +status +" ,payload: "+ pay_load+" ,isbkrnd: "+isbkrnd+" ,status: "+status);
        if (pay_load != null && !isbkrnd && status!=null)
        {
            DataBaseHelper dataBaseHelper;
            dataBaseHelper = new DataBaseHelper(getBaseContext());
            Utility.printLog("notification: inside came intent ");

            //This status becomes 420, when booking got completed by server.
            //This status becomes 25, when booking got accepted through website.
            if (!status.equals("2") )        //(!status.equals("25") && !status.equals("2") && !status.equals("22") && !status.equals("420"))            //(!status.equals("2"))        //THis status (2), means order is already accepted, so no need to show it once again.
            {
                DataBaseGetItemDetailPojo dataBase_getItem_detail_pojo_notfication;
                dataBase_getItem_detail_pojo_notfication = dataBaseHelper.extractFrMyOrderDetail(bid, subid);
                if(dataBase_getItem_detail_pojo_notfication!=null)
                    if (!dataBase_getItem_detail_pojo_notfication.getStatus().equals(status))
                    {
                        Utility.printLog("notification: inside came intent else");
                        dataBaseHelper.updateOrderStatus(status, bid, subid);
                        Toast.makeText(getBaseContext(), pay_load, Toast.LENGTH_SHORT).show();
                        Utility.printLog("notification: inside came intent" + bid + "sub " + subid + "status" + status);
                    }
            }
            else if(!status.isEmpty())
            {
                int statusCode = Integer.parseInt(status.trim());
                switch (statusCode)
                {
                    case 45:        //reachedSoftLimit
                    case 46:        //reachedHardLimit
                    case 47:        //cameOutOfSoftLimit
                    case 48:        //cameOutOfHardLimit
                    case 49:        //newTransaction
                    case 50:        //bothSoftAndHarLimitChanged
                    case 51:        //softLimitBoundryChanged
                    case 52:        //hardLimitBoundryChanged
                        Log.i(TAG, "message action: "+statusCode + "  isWalletFragActive: "+
                                Constants.isWalletFragActive+"  isWalletUpdateCalled: "+Constants.isWalletUpdateCalled);
                        if(Constants.isWalletFragActive && !Constants.isWalletUpdateCalled)
                        {
                            // uncomment it when get wallet data from push also
                            /*final WalletDataChangedEvent walletDataChangedEvent = new Gson().fromJson(msg, WalletDataChangedEvent.class);
                            EventBus.getDefault().post(walletDataChangedEvent);*/
                            EventBus.getDefault().post(new WalletDataChangedEvent());
                        }
                        break;

                    case 53:        //walletEnabled
                    {
                        WalletStatusChangedEvent walletStatusChangedEvent = new WalletStatusChangedEvent();
                        walletStatusChangedEvent.setWalletEnabled(true);
                        EventBus.getDefault().post(walletStatusChangedEvent);
                    }
                    break;

                    case 54:        //walletDisabled
                    {
                        WalletStatusChangedEvent walletStatusChangedEvent = new WalletStatusChangedEvent();
                        walletStatusChangedEvent.setWalletEnabled(false);
                        EventBus.getDefault().post(walletStatusChangedEvent);
                    }
                    break;

                    default:
                        break;
                }
            }
        }

        //sendNotification(pay_load,status,bid,subid,body,title);
        showMessage(pay_load,status,bid,subid,body,title);
    }

    private void showMessage(String msg, String status, String bid, String subid,String body ,String titleNotification) {

        Utility.printLog( "sendNotification  msg " + msg + " bid to send " + bid + "action " + status);
        String title = this.getString(R.string.app_name);
        long when = System.currentTimeMillis();

        if(status==null)
        {
            title=titleNotification;
            msg=body;
        }
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent1 = new Intent(this, NotificationHandler.class);

        mbundle = new Bundle();
        mbundle.putString("status", status);
        mbundle.putString("bid", bid);

        if(status!=null)
        {
            Constants.latesBid=bid;
            Constants.latesstatus=status;
            Constants.latesSubBid=subid;
        }

        Utility.printLog(" bundle in notification " + mbundle + " status " + status + " bid " + bid);
        notificationIntent1.putExtras(mbundle);

        intent = PendingIntent.getActivity(this, 0, notificationIntent1, PendingIntent.FLAG_ONE_SHOT);
      /*  NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(msg);
        inboxStyle.setBigContentTitle(this.getResources().getString(R.string.app_name));*/
        Notification notification;
        notification =  new NotificationCompat.Builder(this).setTicker(titleNotification).setWhen(0).setAutoCancel(true).setContentTitle(titleNotification).setContentIntent(intent).setStyle(new NotificationCompat.BigTextStyle())
       // .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.drawable.ic_stat_name)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_launcher))
                .setPriority(NotificationCompat.PRIORITY_HIGH).setContentText(msg)
        //.setVisibility(NotificationCompat.)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)
                .setContentTitle(this.getResources().getString(R.string.app_name))
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentText(msg)
                .build();
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

        boolean isbkrnd = isApplicationSentToBackground(this);
        if(!isbkrnd)
        {
            doBroadcast();
        }
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.

    /**
     * <h2>sendNotification</h2>
     * <p>
     *     method to Put the message into a notification and post it.
     * </p>
     * @param msg: contains the data to be display in notification
     * @param status: status value i.e action id
     * @param bid: booking id
     * @param subid: sub id of booking
     */
    private void sendNotification(String msg, String status, String bid, String subid,String body ,String titleNotification)
    {
        Utility.printLog( "sendNotification  msg " + msg + " bid to send " + bid + "action " + status);
        String title = this.getString(R.string.app_name);
        long when = System.currentTimeMillis();

        if(status==null)
        {
            title=titleNotification;
            msg=body;
        }
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent1 = new Intent(this, NotificationHandler.class);

        mbundle = new Bundle();
        mbundle.putString("status", status);
        mbundle.putString("bid", bid);

        if(status!=null)
        {
            Constants.latesBid=bid;
            Constants.latesstatus=status;
            Constants.latesSubBid=subid;
        }

        Utility.printLog(" bundle in notification " + mbundle + " status " + status + " bid " + bid);
        notificationIntent1.putExtras(mbundle);

        intent = PendingIntent.getActivity(this, 0, notificationIntent1, PendingIntent.FLAG_ONE_SHOT);

//
//        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
//                .setContentIntent(intent)
//                .setAutoCancel(true)
//                .setOngoing(true)
//                .setSmallIcon(R.drawable.ic_stat_name)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
//                .setTicker(msg)
//                .setWhen(when)
//                .setContentTitle(title)
//                .setContentText(msg)
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText(msg))
//                .setPriority(Notification.PRIORITY_HIGH)
//
//                .setDefaults(Notification.DEFAULT_VIBRATE)
//                ;
//
//
//        if (Build.VERSION.SDK_INT >= 21)
//            notification .setVibrate(new long[0]);
//
////        notification.flags |= Notification.FLAG_ONGOING_EVENT;
////        // Play default notification sound
////        notification.defaults |= Notification.DEFAULT_SOUND;
////        // Vibrate if vibrate is enabled
////        notification.defaults |= Notification.DEFAULT_VIBRATE;
//        notificationManager.notify(0, notification.build());




        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this).setSmallIcon(getNotificationIcon())
               /* .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_launcher))*/

                         .setContentTitle(title).setContentText(msg)
                        .setTicker(msg).setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentIntent(intent)
                        .setDefaults(NotificationCompat.DEFAULT_ALL)
                       .setContentIntent(intent)
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

        if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[0]);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);




        notificationManager.notify( 0, notificationBuilder.build());
        boolean isbkrnd = isApplicationSentToBackground(this);
        if(!isbkrnd)
        {
            doBroadcast();
        }


    }

    /**
     * <h2>isApplicationSentToBackground</h2>
     * <p>
     *     method to check that whether app is in foreground or in background
     * </p>
     * @param context: calling activiyt reference
     * @return: true if app is in background
     */
    public static boolean isApplicationSentToBackground(final Context context)
    {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty())
        {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName()))
            {
                return true;
            }
        }
        return false;
    }


    /**
     * <h2>doBroadcast</h2>
     * <p>
     *     method to broadcast the data
     * </p>
     */
    private void doBroadcast()
    {
        Intent intent = new Intent("com.dayrunner.passenger.booking");
        intent.putExtras(mbundle);
        sendBroadcast(intent);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_stat_name : R.drawable.ic_launcher;
    }
}
