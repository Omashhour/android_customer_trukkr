package com.cargohop.pojos;

/**
 * Created by embed on 29/8/17.
 */

public class CancelReasonDataPojo {
    private String[] reasons;

    private String cancellationFee;

    public String[] getReasons ()
    {
        return reasons;
    }

    public void setReasons (String[] reasons)
    {
        this.reasons = reasons;
    }

    public String getCancellationFee ()
    {
        return cancellationFee;
    }

    public void setCancellationFee (String cancellationFee)
    {
        this.cancellationFee = cancellationFee;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [reasons = "+reasons+", cancellationFee = "+cancellationFee+"]";
    }
}
