package com.cargohop.pojos;

/**
 * <h1>ShipmentFareData</h1>
 * This class is used to parse the fare estimate data
 * @author  embed on 23/5/17.
 */

public class ShipmentFareData {
    private Integer zoneType;

    private String duration;

    private String durationTxt;

    private String pricePerMiles;

    private String dis;

    private String finalAmt;

    private String pricePerMin;

    private String dropId;

    private String pickupId;

    private String estimateId;

    public String getPickupZone() {
        return pickupZone;
    }

    public String getDropZone() {
        return dropZone;
    }

    private String pickupZone;
    private String dropZone;

    public String getEstimateId() {
        return estimateId;
    }

    public String getDropId() {
        return dropId;
    }

    public String getPickupId() {
        return pickupId;
    }

    public Integer getZoneType ()
    {
        return zoneType;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getPricePerMiles ()
    {
        return pricePerMiles;
    }

    public String getDis ()
    {
        return dis;
    }

    public String getFinalAmt ()
    {
        return finalAmt;
    }

    public String getPricePerMin ()
    {
        return pricePerMin;
    }

    public String getDurationTxt() {
        return durationTxt;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [zoneType = "+zoneType+", duration = "+duration+", pricePerMiles = "+pricePerMiles+", dis = "+dis+", finalAmt = "+finalAmt+", pricePerMin = "+pricePerMin+"]";
    }
}