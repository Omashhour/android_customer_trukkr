package com.cargohop.bookingHistory;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.maps.model.Marker;
import com.cargohop.ETA_Pojo.ElementsForEta;
import com.cargohop.bookingFlow.BookingDetailsActivity;
import com.cargohop.customer.MainActivity;
import com.cargohop.interfaceMgr.AssignedBookingsInterface;
import com.cargohop.servicesMgr.PubNubMgr;
import com.cargohop.utility.Alerts;
import com.cargohop.utility.AppTypeface;
import com.cargohop.utility.BitmapCustomMarker;
import com.cargohop.utility.CircleTransform;
import com.cargohop.utility.Constants;
import com.cargohop.utility.PicassoMarker;
import com.cargohop.utility.Scaler;
import com.cargohop.utility.SessionManager;
import com.cargohop.utility.Utility;
import com.cargohop.model.DataBaseHelper;
import com.cargohop.bookingFlow.CancelBookingActivity;
import com.cargohop.controllers.BookingUnAssignedController;
import com.cargohop.eventsHolder.DriverDetailsEvent;
import com.cargohop.customer.R;
import com.cargohop.pojos.Booking_Pojo;
import com.cargohop.pojos.Booking_Shipment_Details;
import com.cargohop.pojos.DataBaseGetItemDetailPojo;
import com.cargohop.pojos.DriverPubnubPojo;
import com.cargohop.pojos.BookingsHistoryListPojo;
import com.cargohop.pojos.BookingDetailsPojo;
import com.cargohop.pojos.RequestBookingPojo;
import com.cargohop.pojos.UnAssignedSharePojo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * <h1>BookingUnAssigned Activity</h1>
 * This class is used to provide the BookingUnAssigned screen, where we can see the screen of unassigned booking status.
 * @author Shubham
 * @since 30 Aug 2017.
 */
public class BookingUnAssigned extends AppCompatActivity implements View.OnClickListener,
        AssignedBookingsInterface {
    private String TAG = "BookingUnAssigned";
    String ent_email, completeData;
    RelativeLayout cancleLayout, back_Button, rl_job_details, call_Rl,rl_action_bar_help;
    TextView deiver_name_Tv, Bid_Tv, title_Tv, tv_vehicle_name, tv_vehicle_no, tv_pick_add, tv_drop_add;
    String driverMapIcon, comingFrom;
    public static GoogleMap googleMap;
    Typeface sans_regular, sans_semibold;
    SessionManager sessionManager;
    PicassoMarker driverMarker = null;
    String driverLat = "", driverLong = "";
    double size[];
    double width;
    double height;
    Resources resources;
    String errMsg;
    private TextView text_drop_address_Tv, text_pick_up_address_Tv_1, tv_job_detail, tv_cancel,
            tv_vehicle_clr, tv_vehicle_plate, tv_msg, tv_message, tv_time;
    ImageView Driver_profile_Iv;
    private LinearLayout bottom_Ll;
    RelativeLayout driver_detail_Rl;
    private SupportMapFragment supportMapFragment;
    private String status = "1";
    private BookingUnAssignedController controller;
    private UnAssignedSharePojo sharePojo;
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private String networkStatus = "", oldNetworkStatus = "";
    private Alerts alerts;
    private ImageView iv_homepage_curr_location;
    private String shipmentDate;
    private Marker driverOnTheWayMarker;
    private LatLng latLngPickup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unassigned_booking);

        sessionManager = new SessionManager(BookingUnAssigned.this);
        controller = new BookingUnAssignedController(this, sessionManager,this);
        sharePojo = new UnAssignedSharePojo();
        BookingDetailsPojo shipmentPojo;
        BookingsHistoryListPojo appointmentPojo;
        resources = getResources();
        alerts = new Alerts();
        Utility.printLog(TAG+"onCreate called : ");

        initialize();

        // fetching all params if email id is null then fetching from databse from
        // order detail
        Bundle bundle = getIntent().getExtras();
        if (bundle.getString("errMsg") != null)
        {
            completeData = bundle.getString("completeData");
            errMsg = bundle.getString("errMsg");
            sharePojo.setBid(bundle.getString("ent_bid"));
            sharePojo.setpaymentTypeText(bundle.getString("PAYMENT_TYPE"));
            Gson gson = new Gson();
            RequestBookingPojo details = gson.fromJson(completeData, RequestBookingPojo.class);
            // setting the time according to the AUG 29, 05:50 PM format.
            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
            SimpleDateFormat fort=new SimpleDateFormat("MMM dd, hh:mm a",Locale.US);
            Date date;
            try
            {
                Utility.printLog("BookingUnAssigned value of appntDate:past: "+details.getEnt_appointment_dt());
                if (details.getEnt_appointment_dt() != null)
                {
                    date = formatter.parse((details.getEnt_appointment_dt()));
                    shipmentDate = fort.format(date);
                    tv_time.setText(shipmentDate);
                }
            }
            catch (Exception e)
            {
                Utility.printLog(TAG+"Exception1 : "+e);
                e.printStackTrace();
            }
            sharePojo.setPickup_Address(details.getEnt_addr_line1());
            sharePojo.setDrop_Address(details.getEnt_drop_addr_line1());
            sharePojo.setDropLat(details.getEnt_drop_lat());
            sharePojo.setDropLong(details.getEnt_drop_long());
            sharePojo.setPickupLat(details.getEnt_lat());
            sharePojo.setPickupLong(details.getEnt_long());
            sharePojo.setRec_name(details.getShipemnt_details()[0].getEnt_receiver_name());
            sharePojo.setRec_phone(details.getShipemnt_details()[0].getEnt_receiver_mobile());
            sharePojo.setItem_name(details.getShipemnt_details()[0].getProduct_name());
            sharePojo.setItem_qty(details.getQuantity());
            sharePojo.setItem_note(details.getShipemnt_details()[0].getAdditional_info());
            sharePojo.setItem_photo(details.getShipemnt_details()[0].getEnt_photo());
            sharePojo.setGoods_type(details.getShipemnt_details()[0].getEnt_category());

            Utility.openDialogWithOkButton(errMsg,false,BookingUnAssigned.this);
        }
        else if(bundle.getString("comingfrom") != null)
        {
            ent_email = bundle.getString("ent_email");
            shipmentPojo = getIntent().getParcelableExtra("shipmentData");
            appointmentPojo = getIntent().getParcelableExtra("orderData");
            comingFrom = bundle.getString("comingFrom");
            sharePojo.setBid(shipmentPojo.getBid());
            sharePojo.setAppnt_Dt(appointmentPojo.getApntDt());
            sharePojo.setPickup_Address(appointmentPojo.getAddrLine1());
            sharePojo.setDrop_Address(appointmentPojo.getDropLine1());
            sharePojo.setDropLat(appointmentPojo.getDrop_lt());
            sharePojo.setDropLong(appointmentPojo.getDrop_lg());
            sharePojo.setPickupLat(appointmentPojo.getPickup_lt());
            sharePojo.setPickupLong(appointmentPojo.getPickup_lg());
            sharePojo.setRec_name(appointmentPojo.getCustomerName());
            sharePojo.setRec_phone(appointmentPojo.getCustomerPhone());
            sharePojo.setItem_name(shipmentPojo.getProductname());
            sharePojo.setItem_note(appointmentPojo.getExtraNotes());
            sharePojo.setItem_qty(shipmentPojo.getQuantity());
            sharePojo.setItem_photo(shipmentPojo.getPhoto());
            sharePojo.setGoods_type(shipmentPojo.getGoodType());
            sharePojo.setDriverPhoneNo(appointmentPojo.getDriverPhone());
            status = appointmentPojo.getStatusCode();

            if (ent_email == null || ent_email.equals("")) {
                DataBaseHelper dataBaseHelper = new DataBaseHelper(BookingUnAssigned.this);
                DataBaseGetItemDetailPojo dataBase_getItem_detail_pojo = dataBaseHelper.extractFrMyOrderDetail(sharePojo.getBid(), "1");
                ent_email = dataBase_getItem_detail_pojo.getDriveremail();
                sharePojo.setBid(dataBase_getItem_detail_pojo.getBid());
                sharePojo.setAppnt_Dt(dataBase_getItem_detail_pojo.getAppdt());
                sharePojo.setDropLat(dataBase_getItem_detail_pojo.getDroplat());
                sharePojo.setDropLong(dataBase_getItem_detail_pojo.getDroplong());
                sharePojo.setPickupLat(dataBase_getItem_detail_pojo.getPickLt());
                sharePojo.setPickupLong(dataBase_getItem_detail_pojo.getPickLong());
                sharePojo.setDriverPhoneNo(dataBase_getItem_detail_pojo.getDriverphone());
            }
        }

        intentFilter = new IntentFilter();
        intentFilter.addAction("com.dayrunner.passenger");
        intentFilter.addAction("com.dayrunner.passenger.booking");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.dayrunner.passenger")) {
                    networkStatus = intent.getStringExtra("STATUS");
                    Utility.printLog("BookingUnAssigned value of network response: " + networkStatus);
                    if (!oldNetworkStatus.equals(networkStatus) && networkStatus.equals("1")) {
                        Utility.printLog("BookingUnAssigned value of network response: resume: " + status);
                        oldNetworkStatus = networkStatus;
                        callService();
                    } else if (!oldNetworkStatus.equals(networkStatus) && networkStatus.equals("0")) {
                        oldNetworkStatus = networkStatus;
                        alerts.showNetworkAlert(BookingUnAssigned.this);
                    }
                }
                else if (intent.getAction().equals("com.dayrunner.passenger.booking"))
                {
                    Utility.printLog("BookingUnAssigned came response:unassigned ");
                    Bundle bundle1 = intent.getExtras();
                    status = bundle1.getString("status");
                    sharePojo.setBid(bundle1.getString("bid"));
                    callService();
                }
            }
        };

        Constants.chnagebooking = true;
        Constants.canclebooking = true;
        Constants.currentbookingPage = "booking";

        Bid_Tv.setText(getString(R.string.bid)+": "+sharePojo.getBid());
        Utility.printLog("BookingUnAssigned vehicle type: "+sessionManager.getVehicleName()+" ,type: "+sessionManager.getVehicleTypes());
        showAddress();
    }

    /**
     * <h2>callService</h2>
     * <p>
     * This method is used for calling the service.
     * </p>
     * to get the latest update from driver
     */
    private void callService()
    {
        Utility.printLog("BookingUnAssigned call service ");
        controller.getDriverDetail(sharePojo.getBid());
    }

    /**
     * <h2>setDriverData</h2>
     * <p>
     * This method is used for setting the Driver data including their photo, name, etc.
     * </p>
     * @param myorder_pojo object of Booking_Pojo
     */
    private void setDriverData(Booking_Pojo myorder_pojo)
    {
        // setting the time according to the AUG 29, 05:50 PM format.
        shipmentDate=Utility.dateFormatter(myorder_pojo.getData().getApntDate());
        tv_time.setText(shipmentDate);

        tv_pick_add.setText(myorder_pojo.getData().getAddrLine1());
        tv_drop_add.setText(myorder_pojo.getData().getDropLine1());

        if(myorder_pojo.getData().getVehicleNumber() != null && !"".equals(myorder_pojo.getData().getVehicleNumber()))
            tv_vehicle_no.setText(myorder_pojo.getData().getVehicleNumber());

        if (myorder_pojo.getData().getDriverPhoto() != null &&
                !myorder_pojo.getData().getDriverPhoto().equals(""))
        {
            String imageUrl = myorder_pojo.getData().getDriverPhoto();
            Picasso.with(BookingUnAssigned.this).
                    load(imageUrl)
                    .placeholder(R.drawable.default_userpic)
                    .resize(resources.getDrawable(R.drawable.default_userpic).getMinimumWidth(), resources.getDrawable(R.drawable.default_userpic).getMinimumHeight())
                    .centerCrop().transform(new CircleTransform())
                    .into(Driver_profile_Iv);
        }
    }

    /**
     * <h2>setDriverStatus</h2>
     * <p>
     * This method is used for setting the Driver status based on the driver changes it.
     * </p>
     * @param status , current status.
     * @param driverName, driver name.
     * @param vehicleName, vehicle name.
     */
    private void setDriverStatus(String status, String vehicleName, String driverName)
    {
        this.status = status;
        if(!status.equals("3") && !status.equals("0") && !status.equals("11"))
            tv_vehicle_name.setText(vehicleName);
        Utility.printLog("BookingUnAssigned value of status: "+status);
        if (!status.equals("0") && !status.equals("1"))
            deiver_name_Tv.setText(driverName);
    }


    /**
     * <h2>updateMap</h2>
     * <p>
     * This method will work to update the map and used for moving to the pickup lat-long and calling when
     * API got called
     * </p>
     * @param pickupLat pickup address latitude
     * @param pickupLong pickup address longitude
     */
    private void updateMap(String pickupLat, String pickupLong)
    {
        LatLng latLng = new LatLng(Double.parseDouble(pickupLat), Double.parseDouble(pickupLong));
        Utility.printLog("BookingUnAssigned pubnub driver url:333: " + driverMapIcon + " , width: "+width+" ,height: "+height);
        if (driverMarker == null ) {
            Utility.printLog("inside driverMarker1");
            driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)));
            Picasso.with(BookingUnAssigned.this)
                    .load(driverMapIcon)
                    .resize((int) width, (int) height)
                    .into(driverMarker);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Utility.printLog("BookingUnAssigned onResume called ");
        if (broadcastReceiver != null)
            registerReceiver(broadcastReceiver, intentFilter);

        EventBus.getDefault().register(this);
        hideCancel(Integer.parseInt(status));

        //to get latest update from driver
        if( Utility.isNetworkAvailable(this))
        {
            callService();
        }
    }

    /**
     * <h3>hideCancel</h3>
     * <p>
     * This method is used for hiding the cancel button from the booking screen,
     * whenever driver reaches to pickup location, customer can't able to cancel the booking.
     * </p>
     * @param status , status.
     */
    private void hideCancel(int status)
    {
        Utility.printLog("BookingUnAssigned value for hide cancel: "+status);
        if (status >= 7)
        {
            cancleLayout.setVisibility(View.GONE);

            LinearLayout.LayoutParams callLayoutParams = (LinearLayout.LayoutParams) call_Rl.getLayoutParams();
            callLayoutParams.weight = 1.5f;
            call_Rl.setLayoutParams(callLayoutParams);

            LinearLayout.LayoutParams jobLayoutParams = (LinearLayout.LayoutParams) rl_job_details.getLayoutParams();
            jobLayoutParams.weight = 1.5f;
            rl_job_details.setLayoutParams(jobLayoutParams);
        }
    }

    /**
     * <h2>initialize</h2>
     * <p>
     * initialize all UI elements and setting onclick event
     * </p>
     */
    private void initialize() {
        size = Scaler.getScalingFactor(BookingUnAssigned.this);
        width = size[0] * 45;
        height = size[1] * 45;
        Bid_Tv =  findViewById(R.id.Bid_Tv);

        if(Utility.isRTL())
        {
            ImageView ivBackBtn =  findViewById(R.id.ivBackArrow);
            ivBackBtn.setRotation((float) 180.0);
        }

        title_Tv =  findViewById(R.id.tvToolBarTitle);
        title_Tv.setText(getString(R.string.track_live_tasks));
        title_Tv.setTypeface(AppTypeface.getInstance(this).getPro_narMedium());

        tv_vehicle_name =  findViewById(R.id.tv_vehicle_name);
        tv_vehicle_no =  findViewById(R.id.tv_vehicle_no);
        tv_pick_add =  findViewById(R.id.tv_pick_add);
        tv_drop_add =  findViewById(R.id.tv_drop_add);
        deiver_name_Tv =  findViewById(R.id.deiver_name_Tv);
        text_pick_up_address_Tv_1 =  findViewById(R.id.text_pick_up_address_Tv_1);
        text_drop_address_Tv =  findViewById(R.id.text_drop_address_Tv);
        tv_job_detail =  findViewById(R.id.tv_job_detail);
        tv_cancel =  findViewById(R.id.tv_cancel);
        back_Button =  findViewById(R.id.rlBackArrow);
        cancleLayout =  findViewById(R.id.rl_cancel);
        call_Rl =  findViewById(R.id.call_Rl);
        rl_job_details =  findViewById(R.id.rl_job_details);
        Driver_profile_Iv =  findViewById(R.id.Driver_profile_Iv);
        tv_vehicle_clr =  findViewById(R.id.tv_vehicle_clr);
        tv_vehicle_plate =  findViewById(R.id.tv_vehicle_plate);
        bottom_Ll = findViewById(R.id.bottom_Ll);
        driver_detail_Rl =  findViewById(R.id.driver_detail_Rl);
        tv_msg =  findViewById(R.id.tv_msg);

        rl_action_bar_help =  findViewById(R.id.rlToolBarEnd);
        rl_action_bar_help.setVisibility(View.VISIBLE);
        rl_action_bar_help.setOnClickListener(this);

        TextView tvToolBarEnd =  findViewById(R.id.tvToolBarEnd);
        tvToolBarEnd.setText(getString(R.string.help));

        tv_message =  findViewById(R.id.tv_message);
        iv_homepage_curr_location = findViewById(R.id.iv_homepage_curr_location);

        iv_homepage_curr_location.setOnClickListener(this);

        tv_time =  findViewById(R.id.tv_time);
        tv_msg.setText(getString(R.string.booking_requested));

        setTypeFace();
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_booking);
    }

    @Override
    protected void onStart() {
        super.onStart();
        cancleLayout.setOnClickListener(this);
        call_Rl.setOnClickListener(this);
        back_Button.setOnClickListener(this);
        rl_job_details.setOnClickListener(this);
    }

    /**
     * <h2>showAddress</h2>
     * <p>
     * This method is used for showing address.
     * </p>
     */
    private void showAddress()
    {
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gMap) {
                googleMap = gMap;
                mapMethod();
            }
        });

        if (comingFrom != null && comingFrom.equals("tracking")) {

            cancleLayout.setVisibility(View.GONE);
            Constants.chnagebooking = false;
            Constants.currentbookingPage = "";
        }

        tv_pick_add.setText(sharePojo.getPickup_Address());
        tv_drop_add.setText(sharePojo.getDrop_Address());
    }

    /**
     * <h2>mapMethod</h2>
     * <p>
     * This method is used for initialising the map.
     * </p>
     */
    private void mapMethod()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        drawIcon();
    }

    /**
     * <h2>setTypeFace</h2>
     * <p>
     * This method is used for setting the Typeface for the text.
     * </p>
     */
    private void setTypeFace()
    {
        AppTypeface appTypeface = AppTypeface.getInstance(this);
        sans_semibold = appTypeface.getSans_semiBold();
        sans_regular = appTypeface.getSans_regular();
        text_pick_up_address_Tv_1.setTypeface(sans_semibold);
        text_drop_address_Tv.setTypeface(sans_semibold);
        tv_job_detail.setTypeface(sans_semibold);
        tv_cancel.setTypeface(sans_semibold);
        deiver_name_Tv.setTypeface(sans_semibold);
        Bid_Tv.setTypeface(sans_semibold);
        title_Tv.setTypeface(sans_semibold);
        tv_vehicle_name.setTypeface(sans_semibold);
        tv_vehicle_no.setTypeface(sans_semibold);
        tv_pick_add.setTypeface(sans_regular);
        tv_drop_add.setTypeface(sans_regular);
        tv_time.setTypeface(sans_regular);
        tv_message.setTypeface(sans_regular);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.call_Rl:
                Utility.printLog("BookingUnAssigned value of number while clling: "+sharePojo.getDriverPhoneNo());
                Intent callintent = new Intent(Intent.ACTION_CALL);
                callintent.setData(Uri.parse("tel:" + sharePojo.getDriverPhoneNo()));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callintent);
                break;

            case R.id.rl_cancel:
                Intent intent = new Intent(this, CancelBookingActivity.class);
                intent.putExtra("ent_bid", sharePojo.getBid());
                intent.putExtra("ent_appnt_dt", sharePojo.getAppnt_Dt());
                intent.putExtra("ent_email", ent_email);
                startActivity(intent);
                break;

            case R.id.rlBackArrow:
                onBackPressed();
                break;

            case R.id.rlToolBarEnd:
                Utility.startChatActivity(this, sessionManager.username(), sessionManager.getCustomerEmail());
                break;

            case R.id.rl_job_details:
                Intent intent1 = new Intent(this, BookingDetailsActivity.class);
                intent1.putExtra("completeData", sharePojo);
                startActivity(intent1);
                break;

            case R.id.iv_homepage_curr_location:
            {
                Utility.printLog("BookingUnAssigned curr latlong in assigned onclick ");
                moveCameraPos(Double.parseDouble(sessionManager.getlatitude()), Double.parseDouble(sessionManager.getlongitude()));
                break;
            }
        }
    }

    /**
     *<h2>moveCameraPos</h2>
     * <p>
     *     method to update the marker
     * </p>
     * @param newLat: selected addres latitude
     * @param newLong: selected addres longitude
     */
    private void moveCameraPos(double newLat, double newLong)
    {
        Utility.printLog("BookingUnAssigned GoogleMap moveCameraPos newLat: "+newLat+" newLong: "+newLong);
        if(googleMap == null)
            return;

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(newLat, newLong)).zoom(17.00f).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    /**
     * <h2>drawIcon</h2>
     * <p>
     * This method is used for drawing the map icon,
     * and we are drawing this icon from ourside whenever we got response from API,
     * and its represent to the Pick Up and Drop Off location.
     * </p>
     */
    private void drawIcon()
    {
        LatLng latLngDrop=new LatLng(Double.parseDouble(sharePojo.getDropLat()),Double.parseDouble(sharePojo.getDropLong()));
        latLngPickup=new LatLng(Double.parseDouble(sharePojo.getPickupLat()),Double.parseDouble(sharePojo.getPickupLong()));

        googleMap.addMarker(new MarkerOptions().position(latLngPickup)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_map_pin_icon_green))
        );

        googleMap.addMarker(new MarkerOptions().position(latLngDrop)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red))
        );
    }

    /**
     * subscribe the pubnub channel and sending response to plotdriver method
     * @param driverDetailsEvent object of DriverDetailsEvent
     */
    @Subscribe (threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DriverDetailsEvent driverDetailsEvent)
    {
        Utility.printLog("BookingUnAssigned onMessageEvent called UnAssigned: "+driverDetailsEvent.getDriver_pubnub_pojo().toString());
        plotDriver(driverDetailsEvent.getDriver_pubnub_pojo());
    }

    /**
     * <h2>plotDriver</h2>
     * <p>
     * live tracking of driver on map. changeing map marker if the driver lat long change
     * </p>
     * @param driver_pubnub_pojo, contains the actual pubnub message.
     */
    private void plotDriver(DriverPubnubPojo driver_pubnub_pojo) {
        try {
            Utility.printLog("BookingUnAssigned pubnub driver getLg changed " + driver_pubnub_pojo.getLg()
                    +"getLt "+driver_pubnub_pojo.getLt());
            if (sharePojo.getBid().equals(driver_pubnub_pojo.getBid().trim()))
            {
                //driver cancels the booking
                Gson gson=new Gson();
                String driverDataResult = gson.toJson(driver_pubnub_pojo, DriverPubnubPojo.class);
                JSONObject jsonObject=new JSONObject(driverDataResult);
                if(jsonObject.has("status"))
                {
                    if(driver_pubnub_pojo.getStatus().equals("4"))
                    {
                        Utility.printLog(TAG+"booking cancelled ");
                        Utility.openDialogWithOkButton(driver_pubnub_pojo.getMsg(),true,BookingUnAssigned.this);
                    }
                }

                tv_message.setVisibility(View.GONE);            //JUST HIDE MIDDLE PINK ICON AND BLUE MESSAGE BAR.
                tv_time.setVisibility(View.GONE);
                iv_homepage_curr_location.setVisibility(View.VISIBLE);

                Bid_Tv.setText(getString(R.string.bid) + " : " + driver_pubnub_pojo.getBid());
                if (driver_pubnub_pojo.getBid() != null) {
                    Utility.printLog("BookingUnAssigned pub inside messge onthe way   detail:plotDriver: " + driver_pubnub_pojo.getA() + " ,status: " + driver_pubnub_pojo.getSt());
                    switch (driver_pubnub_pojo.getSt()) {
                        case 11:
                            Utility.finishAndRestartMainActivity(this);
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 16:
                            //tv_message.setVisibility(View.GONE);
                            if(Integer.parseInt(status) != driver_pubnub_pojo.getSt())
                                performUIChanges(driver_pubnub_pojo);
                            break;

                        case 10:
                            startRatingActivity(driver_pubnub_pojo.getBid());
                            break;
                    }
                    if (driver_pubnub_pojo.getLt() != null) {
                        driverLat = driver_pubnub_pojo.getLt();
                        driverLong = driver_pubnub_pojo.getLg();

                        drawIcon();        //This method is used to plot the map icon.
                        LatLng latLng = new LatLng(Double.parseDouble(driver_pubnub_pojo.getLt()),
                                Double.parseDouble(driver_pubnub_pojo.getLg()));
                        setMarker(latLng);

                        if(driver_pubnub_pojo.getSt()==6)
                            controller.getETAOfDriver(driverLat,driverLong,sharePojo.getPickupLat()
                                    ,sharePojo.getPickupLong());
                        else {
                            if(driverOnTheWayMarker!=null)
                                driverOnTheWayMarker.remove();
                        }
                    }
                    hideCancel(driver_pubnub_pojo.getSt());
                }
            }
        } catch (Exception e) {
            Utility.printLog(TAG+"Exception3 : "+e);
            e.printStackTrace();
        }
    }

    /**
     * <h2>performUIChanges</h2>
     * <p>
     * This method is used to do the UI changes on our screen.
     * </p>
     * @param driver_pubnub_pojo object of DriverPubnubPojo
     */
    private void performUIChanges(DriverPubnubPojo driver_pubnub_pojo)
    {
        Utility.printLog("BookingUnAssigned value of pubnub response:  "+driver_pubnub_pojo.getA()+" ,stat: "+driver_pubnub_pojo.getSt()+" ,msg: "+driver_pubnub_pojo.getMsg()+" ,bid: "+driver_pubnub_pojo.getBid());
        bottom_Ll.setWeightSum(3f);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);//(LinearLayout.LayoutParams) bottom_Ll.getLayoutParams(); //or create new LayoutParams...

        lParams.weight = 1.0f;
        call_Rl.setVisibility(View.VISIBLE);
        call_Rl.setLayoutParams(lParams);
        sharePojo.setDriverPhoneNo(driver_pubnub_pojo.getMobile());
        rl_job_details.setLayoutParams(lParams);
        cancleLayout.setLayoutParams(lParams);

        bottom_Ll.removeAllViews();

        bottom_Ll.addView(call_Rl);
        bottom_Ll.addView(rl_job_details);
        bottom_Ll.addView(cancleLayout);

        Driver_profile_Iv.setVisibility(View.VISIBLE);
        tv_vehicle_clr.setVisibility(View.GONE);
        tv_vehicle_plate.setVisibility(View.GONE);

        tv_msg.setText(driver_pubnub_pojo.getMsg());
        if (driver_pubnub_pojo.getSt() == 6)
        {
            deiver_name_Tv.setText(driver_pubnub_pojo.getDriverName());
            tv_vehicle_name.setText(driver_pubnub_pojo.getVehicelType());
            tv_vehicle_no.setText(driver_pubnub_pojo.getVehiclenumber());

            if (driver_pubnub_pojo.getDriverImage() != null && !driver_pubnub_pojo.getDriverImage().equals(""))
            {
                String imageurl = driver_pubnub_pojo.getDriverImage();
                Picasso.with(BookingUnAssigned.this).
                        load(imageurl)
                        .placeholder(R.drawable.default_userpic)
                        .resize(resources.getDrawable(R.drawable.default_userpic).getMinimumWidth(), resources.getDrawable(R.drawable.default_userpic).getMinimumHeight())
                        .centerCrop().transform(new CircleTransform())
                        .into(Driver_profile_Iv);
            }
        }
    }

    /**
     * <h2>startRatingActivity</h2>
     * <p>
     *     method to start rating Activity
     * </p>
     * @param bid: booking id
     */
    private void startRatingActivity(String bid)
    {
        Utility.printLog("BookingUnAssigned startRatingActivity: "+bid);
        if (!PubNubMgr.isReceiptActVisible())
        {
            PubNubMgr.setIsReceiptActVisible(true);
            Intent intent = new Intent(this, ReceiptActivity.class);
            intent.putExtra("bid", bid);
            finish();
            startActivity(intent);
        }
    }

    /**
     * <h2>setMarker</h2>
     * <p>
     * This method is used to set the driver marker
     * </p>
     * @param latLng LatLong object of the driver marker location
     */
    private void setMarker(LatLng latLng){
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        if(driverMarker!=null){
           driverMarker.getmMarker().setPosition(latLng);
        }else {
            Utility.printLog("inside driverMarker2");
            driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)));
            Utility.printLog("BookingUnAssigned pubnub driver url " + driverMapIcon);
            Picasso.with(BookingUnAssigned.this).
                    load(driverMapIcon)
                    .resize((int) width, (int) height)
                    .into(driverMarker);
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BookingUnAssigned.this, MainActivity.class);
        Constants.bookingFlag = true;
        Constants.chnagebooking=false;
        Constants.canclebooking=false;
        Constants.currentbookingPage="";
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
    }

    @Override
    public void onSuccess(String result) {
        Gson gson = new Gson();
        Booking_Shipment_Details shipment_details[];
        Booking_Pojo myorder_pojo;
        myorder_pojo = gson.fromJson(result, Booking_Pojo.class);
        if (myorder_pojo.getErrFlag().equals("0"))
        {
            String statusCode = myorder_pojo.getData().getStatusCode();
            Utility.printLog("BookingUnAssigned call service onSuccess statusCode: "+statusCode);
            if(!statusCode.isEmpty())
            {
                switch (Integer.parseInt(statusCode)) {
                    case 11:
                        Utility.finishAndRestartMainActivity(this);
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 16:
                        tv_message.setVisibility(View.GONE);
                        break;

                    case 10:
                        startRatingActivity(myorder_pojo.getData().getBid());
                        break;

                    case 4://booking cancelled by driver
                        Utility.openDialogWithOkButton(myorder_pojo.getData().getStatus(),
                                true,BookingUnAssigned.this);
                        break;
                }
            }
            tv_msg.setText(myorder_pojo.getData().getStatus());
            shipment_details = myorder_pojo.getData().getShipemntDetails();
            sharePojo.setHelpers(myorder_pojo.getData().getHelpers());
            sharePojo.setItem_photo(myorder_pojo.getData().getShipemntDetails()[0].getPhoto());
            sharePojo.setGoods_type(myorder_pojo.getData().getShipemntDetails()[0].getGoodType());
            setDriverData(myorder_pojo);
            setDriverStatus(shipment_details[0].getStatus(), myorder_pojo.getData().getVehicleTypeName(), myorder_pojo.getData().getDriverName());
            Utility.printLog("BookingUnAssigned value of status: "+shipment_details[0].getStatus()+" , lat: "+
                    myorder_pojo.getData().getDriverLastLocation().getLatitude()+" ,lng: " +
                    myorder_pojo.getData().getDriverLastLocation().getLongitude()+" image "+
                    myorder_pojo.getData().getVehicleTypeImage());
            if(myorder_pojo.getData().getAppt_type().equals("2"))
            {
                tv_time.setText(getString(R.string.requested_For)+ " " + shipmentDate);
            }
            driverMapIcon=myorder_pojo.getData().getVehicleTypeImage();

            LatLng latLng;
            try
            {
                Utility.printLog(TAG+"driver lat "+myorder_pojo.getData().getDriverLastLocation()
                        .getLatitude()+"long "+ myorder_pojo.getData().getDriverLastLocation().getLongitude());
                latLng=new LatLng(Double.parseDouble(myorder_pojo.getData().getDriverLastLocation()
                        .getLatitude()), Double.parseDouble(myorder_pojo.getData().
                        getDriverLastLocation().getLongitude()));
            }
            catch (Exception e)
            {
                Utility.printLog(TAG+"driver Exception "+e);
                latLng=new LatLng(Double.parseDouble(shipment_details[0].getLocation().getLat()),
                        Double.parseDouble(shipment_details[0].getLocation().getLog()));
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));

            if ((!shipment_details[0].getStatus().equals("0") && !shipment_details[0].getStatus().equals("1")                                                )) {
                if ((shipment_details[0].getLocation().getLat() != null &&
                        shipment_details[0].getLocation().getLog() != null)) {
                    updateMap(shipment_details[0].getLocation().getLat(),
                            shipment_details[0].getLocation().getLog());
                }
            }
            drawIcon();

            if(myorder_pojo.getData().getShipemntDetails()[0].getStatus().equals("6"))
                controller.getETAOfDriver(myorder_pojo.getData().
                                getDriverLastLocation().getLatitude(),myorder_pojo.getData()
                                .getDriverLastLocation().getLongitude(),sharePojo.getPickupLat(),
                        sharePojo.getPickupLong());
        }
    }

    @Override
    public void OnGettingOfETA(ArrayList<ElementsForEta> etaElementsOfDriver) {
        int duration= (int) (Math.round(Double.parseDouble(etaElementsOfDriver
                .get(0).getDuration().getValue())/60));

        if(duration==0)
            duration=1;

        BitmapCustomMarker driverOnTheWAyCustomMarker=new BitmapCustomMarker(this, duration+"");
        if(driverOnTheWAyCustomMarker.createBitmap()!=null)
        {
            if(driverOnTheWayMarker!=null)
                driverOnTheWayMarker.remove();

            driverOnTheWayMarker=googleMap.addMarker(new MarkerOptions().position(latLngPickup)
                    .title("PICK").flat(false)
                    .icon(BitmapDescriptorFactory.fromBitmap(driverOnTheWAyCustomMarker.createBitmap())));
        }
    }
}
